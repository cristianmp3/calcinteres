<%-- 
    Document   : Correcto
    Created on : 07-abr-2020, 1:05:09
    Author     : Cristian Martinez
--%>

<%@page import="total.Total " %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    
    Total t1 = (Total)request.getSession().getAttribute("total1");
    
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calculado</title>
    </head>
    <body>
        <h2>Calculo exitoso</h2>
        <p>Capital: <%= t1.getCapital()%> <p>
        <p>Años: <%= t1.getAños()%> </p>
        <p>interés: <%= t1.getInteres()%> </p>
        <p>total:<%= t1.getResultado()%> </p>
    </body>
</html>
